import UIKit
import MSAL

/// 😃 A View Controller that will respond to the events of the Storyboard.
class LoginViewController: UIViewController, UITextFieldDelegate, URLSessionDelegate {
    
    // Update the below to your client ID you received in the portal. The below is for running the demo only
    let kClientID = "5301a170-53af-4a3f-a70a-a06a1dc9ff04"
    
    // These settings you don't need to edit unless you wish to attempt deeper scenarios with the app.
    let kGraphURI = "https://graph.microsoft.com/v1.0/me/"
    let kScopes: [String] = ["https://graph.microsoft.com/user.read"]
    let kAuthority = "https://login.microsoftonline.com/common"
    
    var accessToken = String()
    var applicationContext : MSALPublicClientApplication?
    
    @IBOutlet weak var loggingText: UITextView!
    @IBOutlet weak var signoutButton: UIButton!
    
    /**
     Setup public client application in viewDidLoad
     */
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        do {
            
            /**
             Initialize a MSALPublicClientApplication with a given clientID and authority
             - clientId:            The clientID of your application, you should get this from the app portal.
             - authority:           A URL indicating a directory that MSAL can use to obtain tokens. In Azure AD
             it is of the form https://<instance/<tenant>, where <instance> is the
             directory host (e.g. https://login.microsoftonline.com) and <tenant> is a
             identifier within the directory itself (e.g. a domain associated to the
             tenant, such as contoso.onmicrosoft.com, or the GUID representing the
             TenantID property of the directory)
             - error                The error that occurred creating the application object, if any, if you're
             not interested in the specific error pass in nil.
             */
            
            guard let authorityURL = URL(string: kAuthority) else {
                self.loggingText.text = "Unable to create authority URL"
                return
            }
            
            let authority = try MSALAuthority(url: authorityURL)
            self.applicationContext = try MSALPublicClientApplication(clientId: kClientID, authority: authority)
            
        } catch let error {
            self.loggingText.text = "Unable to create Application Context \(error)"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if CheckConnection.Connection(){
            
            if self.currentAccount() == nil{
                super.viewWillAppear(animated)
            } else{
                DispatchQueue.main.async {
                    self.performSegue(withIdentifier: "loginsegue", sender: self)
                }
            }
            
        }
        else{
            
            self.Alert(Message: "Your Device is not connected with internet")
        }
    }
        
        //signoutButton.isEnabled = !self.accessToken.isEmpt
    
    /**
     This button will invoke the authorization flow.
     */
    
    @IBAction func callGraphButton(_ sender: UIButton) {
        
        if self.currentAccount() == nil {
            // We check to see if we have a current logged in account.
            // If we don't, then we need to sign someone in.
            self.acquireTokenInteractively()
            //self.performSegue(withIdentifier: "LoginSegue", sender: self)
           

        } else {
            //print("hey que pasa")
            //self.acquireTokenSilently()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "loginsegue", sender: self)
            }
            
        }
    }
    
    func acquireTokenInteractively() {
        
        guard let applicationContext = self.applicationContext else { return }
        
        applicationContext.acquireToken(forScopes: kScopes) { (result, error) in
            
            if let error = error {
                
                //self.updateLogging(text: "Could not acquire token: \(error)")
                return
            }
            
            guard let result = result else {
                
                //self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken!
            //self.updateLogging(text: "Access token is \(self.accessToken)")
            //self.updateSignoutButton(enabled: true)
            //self.getContentWithToken()
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "loginsegue", sender: self)
            }
            //self.signoutButton.isEnabled = true
        }
    }
    
    func acquireTokenSilently() {
        
        guard let applicationContext = self.applicationContext else { return }
        
        /**
         Acquire a token for an existing account silently
         - forScopes:           Permissions you want included in the access token received
         in the result in the completionBlock. Not all scopes are
         guaranteed to be included in the access token returned.
         - account:             An account object that we retrieved from the application object before that the
         authentication flow will be locked down to.
         - completionBlock:     The completion block that will be called when the authentication
         flow completes, or encounters an error.
         */
        
        applicationContext.acquireTokenSilent(forScopes: kScopes, account: self.currentAccount()) { (result, error) in
            
            if let error = error {
                
                let nsError = error as NSError
                
                // interactionRequired means we need to ask the user to sign-in. This usually happens
                // when the user's Refresh Token is expired or if the user has changed their password
                // among other possible reasons.
                if (nsError.domain == MSALErrorDomain
                    && nsError.code == MSALErrorCode.interactionRequired.rawValue) {
                    
                    DispatchQueue.main.async {
                        self.acquireTokenInteractively()
                    }
                    
                } else {
                    //self.updateLogging(text: "Could not acquire token silently: \(error)")
                }
                
                return
            }
            
            guard let result = result else {
                
                //self.updateLogging(text: "Could not acquire token: No result returned")
                return
            }
            
            self.accessToken = result.accessToken!
            //self.updateLogging(text: "Refreshed Access token is \(self.accessToken)")
            //self.signoutButton.isEnabled = true
            //self.getContentWithToken()
        }
    }
    
    func currentAccount() -> MSALAccount? {
        
        guard let applicationContext = self.applicationContext else { return nil }
        
        // We retrieve our current account by getting the first account from cache
        // In multi-account applications, account should be retrieved by home account identifier or username instead
        do {
            
            let cachedAccounts = try applicationContext.allAccounts()
            
            if !cachedAccounts.isEmpty {
                return cachedAccounts.first
            }
            
        }
        catch let error as NSError {
            
            //self.updateLogging(text: "Didn't find any accounts in cache: \(error)")
        }
        
        return nil
}
   // @IBAction func signoutButton(_ sender: UIButton) {
        
   //     guard let applicationContext = self.applicationContext else { return }
        
   //     guard let account = self.currentAccount() else { return }
        
    //    do {
            
            /**
             Removes all tokens from the cache for this application for the provided account
             - account:    The account to remove from the cache
             */
            
     //       try applicationContext.remove(account)
      //      self.loggingText.text = ""
       //     self.signoutButton.isEnabled = false
            
        //} catch let error as NSError {
            
            //self.updateLogging(text: "Received error signing account out: \(error)")
        //}////
    //}
    
    func Alert (Message: String){
        
        let alert = UIAlertController(title: "Alert", message: Message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
}
